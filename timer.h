#ifndef _TIMER_H
#define _TIMER_H

using namespace std;

vector<int> arg_day(vector<int>& days);
vector<int> set_day(vector<int>& days);
void timer(int h, int min, vector<int> days);

#endif //_TIMER_H