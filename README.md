SMART HOME ALARM CLOCK 

Description: The alarm clock plays quiet or rhythmic music depending on the temperature in the room.

Hardware: Raspberry Pi. 

Software: Raspberry Pi OS. 

Instructons: 
1. To compile: cd ./build; cmake ..; make; 

2. Paste in terminal: 

export LC_ALL=C; 
gpio -g mode 26 out; gpio -g write 26 1; 
gpio -g mode 12 out; gpio -g write 12 1; 
gpio -g mode 19 out; gpio -g write 19 1; 
gpio -g write 26 0; 
gpio -g write 12 0; 
gpio -g write 19 0 

3. Go to the 'audio' folder. 
Place the music you want to play when the room temperature is below 23 degrees in the 'chill' folder, 
and the music you want to play when the temperature is higher in the 'warm' folder. 

4. To run: ./alarm [hh] [mm] [d] 

[hh] - hour; 
[mm] - minutes; 
[d] - days: 0 - every day, 1 - Monday, 2 - Tuesday, 3 - Wednesday, 4 - Thursday, 5 - Friday, 6 - Saturday, 7 - Sunday. 
