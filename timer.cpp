#include <iostream>		// cout
#include <iomanip>		// put_time
#include <thread>		// sleep_until, sleep_for
#include <vector>
#include <algorithm>
#include "timer.h"
#include "peripherals.h"

using namespace std;


vector<int> arg_day(vector<int>& days_tmp){
	
	vector<int> days {9, 9, 9, 9, 9, 9, 9};

	for(int n=0; n<7; n++){
		int day = days_tmp[n];
		switch( day ){
			case(0): days = {0, 1, 2, 3, 4, 5, 6}; break;
			case(1): days[1] = 1; break;
			case(2): days[2] = 2; break;
			case(3): days[3] = 3; break;
			case(4): days[4] = 4; break;
			case(5): days[5] = 5; break;
			case(6): days[6] = 6; break;
			case(7): days[0] = 0; break;
			case(9): break;
			default:
				cerr << "Incorrect day number. 1-7 Monday-Sunday, 0 - everyday" << endl;
				exit(1);
		}//switch
	}//for

	vector<int>::iterator it;
	it  = find(days.begin(), days.end(), 9);
	if( it != days.end() ){
		cout << "\nAlarm is scheduled for: ";
		if(days[1] == 1) cout << "Monday ";
		if(days[2] == 2) cout << "Tuesday ";
		if(days[3] == 3) cout << "Wednesday ";
		if(days[4] == 4) cout << "Thursday ";
		if(days[5] == 5) cout << "Friday ";
		if(days[6] == 6) cout << "Saturday ";
		if(days[0] == 0) cout << "Sunday ";
		if (days[0] == 9 && days[1] == 9 && days[2] == 9 && 
		days[3] == 9 && days[4] == 9 && days[5] == 9 && days[6] == 9){
			cerr << "Incorrect day number. 1-7 Monday-Sunday, 0 - everyday" << endl;
			exit(1);
		}
		cout << endl;
	}else{
		cout << "The alarm is scheduled every day" << endl;
	}

	return days;
}//arg_day



vector<int> set_day(vector<int>& days){
	cout << "Enter the numbers of days you want the wake-up, e.g. \"1 3\" = Monday, Wednesday" << endl;
	cout << "0 - every day	" << "1 - Monday	" << "2 - Tuesday" << endl
		<< "3 - Wednesday	" << "4 - Thursday	" << "5 - Friday" << endl 
		<< "6 - Saturday	" << "7 - Sunday" << endl;
	days = {9, 9, 9, 9, 9, 9, 9};
	int day;
	while(1){
		cin >> day;
		switch(day){
			case(0): days = {0, 1, 2, 3, 4, 5, 6}; break;
			case(1): days[1] = 1; break;
			case(2): days[2] = 2; break;
			case(3): days[3] = 3; break;
			case(4): days[4] = 4; break;
			case(5): days[5] = 5; break;
			case(6): days[6] = 6; break;
			case(7): days[0] = 0; break;
			default:
				cerr << "No number was given or incorrect day number" << endl;
				exit(1);
		}//switch
		if(getchar()=='\n' || day == 0)break;
	}//while

	vector<int>::iterator it;
	it  = find(days.begin(), days.end(), 9);
	if( it != days.end() ){
		cout << "\nAlarm is scheduled for: ";
		if(days[1] == 1) cout << "Monday ";
		if(days[2] == 2) cout << "Tuesday ";
		if(days[3] == 3) cout << "Wednesday ";
		if(days[4] == 4) cout << "Thursday ";
		if(days[5] == 5) cout << "Friday ";
		if(days[6] == 6) cout << "Saturday ";
		if(days[0] == 0) cout << "Sunday ";
		cout << endl;
	}else{
		cout << "The alarm is scheduled every day" << endl;
	}

	return days;
}//set_day



void timer(int h, int min, vector<int> days){

	for(;;){
		using chrono::system_clock; // sleep_until
		time_t now;
		struct tm ptm;
		time(&now);
		ptm = *localtime(&now);

		cout << "Current time: " << put_time(&ptm,"%X") << '\n';

		ptm.tm_hour = h; ptm.tm_min = min; ptm.tm_sec = 0; //setup wake-up time

		cout << "The alarm clock is scheduled to: " << put_time(&ptm,"%X") << '\n';

		long double differ;
		differ = difftime(now, mktime(&ptm));
		cout << "There are " << differ << " seconds before(if-)/after(if+) the alarm time" << endl;

		vector<int>::iterator it;
		it  = find(days.begin(), days.end(), ptm.tm_wday);

		if(differ == 0){ //the wake-up time!
			if( it != days.end() ){
				pthread_t thread_one, thread_two, thread_three;	// declare two thread 
				pthread_create(&thread_one, NULL, butt, NULL);	// create thread_one 
				pthread_create(&thread_two, NULL, led, NULL);	// create thread_two 
				pthread_create(&thread_three, NULL, play_list, NULL);	// create thread_three
				
				pthread_join(thread_one, NULL); // waiting for when thread_one is completed 
				pthread_join(thread_two, NULL); // waiting for when thread_two is completed
				pthread_join(thread_three, NULL); // waiting for when thread_three is completed

				this_thread::sleep_for(1000ms); //precision
			}else{
				cout << "The alarm was not scheduled for today." << endl;
				this_thread::sleep_for(1000ms); //precision
			}
		}else if(differ < 0){ //now is before the wake-up time
			cout << "Waiting for the alarm..." << endl;
			this_thread::sleep_until(system_clock::from_time_t (mktime(&ptm)));
			this_thread::sleep_for(999ms); //precision
		}else if(differ > 0){ //now is after the wake-up time
			cout << "Today is after the wake-up time " << put_time(&ptm,"%X") << endl;
			ptm.tm_hour = 23; ptm.tm_min = 59; ptm.tm_sec = 59; //time to rebuild the program
			cout << "Waiting for: " << put_time(&ptm,"%X") << " to rebuild the program" << endl;
			this_thread::sleep_until(system_clock::from_time_t (mktime(&ptm)));
			cout << "Waiting 2 seconds" << endl;
			this_thread::sleep_for(2999ms);
		}else{
			cerr << "Error time\n\n";
		}//if, else
		cout << endl;
	}//for

}//timer
