#include <iostream>		// cout
#include <stdio.h>
#include <vector>
#include <algorithm>
#include "timer.h"		//set_day, timer
#include "peripherals.h"

using namespace std;

int main(int argc, char** argv){

	int hour, minute;
	vector<int> days {0, 1, 2, 3, 4, 5, 6};

	if(argc == 3){ //user entered the hour and minute to wake up
		if(atoi(argv[1]) >= 0 && atoi(argv[1]) <= 23 && atoi(argv[2]) >= 0 && atoi(argv[2]) <= 59){
			hour = atoi(argv[1]);
			minute = atoi(argv[2]);
			set_day(days);
		}else{
			cerr << "Invalid calling arguments. Correct input: ./alarm or ./alarm HH MM" << endl;
			exit(1);
		}
	}else if(argc > 3){ //user entered the hour, minute and days to wake-up
		if(atoi(argv[1]) >= 0 && atoi(argv[1]) <= 23 && atoi(argv[2]) >= 0 && atoi(argv[2]) <= 59){
			hour = atoi(argv[1]);
			minute = atoi(argv[2]);
			vector<int> days_tmp {9, 9, 9, 9, 9, 9, 9};
			
			for(int n = 4; n <= 14; n++){ //which days as arguments
				if(argc >= n){
					days_tmp[n-4] = atoi(argv[n-1]);
				}
			}
			days = arg_day(days_tmp);
		}else{
			cerr << "Invalid calling arguments. Correct input: ./alarm or ./alarm HH MM or ./alarm HH MM D (D - days: 0-every day, 1-7: monday-sunday)" << endl;
			exit(1);
		}
	}else if(argc == 1){ //user hasnt entered time
		cout << "Enter the alarm hour and minute [HH MM]: ";
		cin >> hour >> minute;
		if(hour < 0 || hour > 23 || minute < 0 || minute > 59){
			cerr << "Invalid value hour/minute" << endl;
			exit(1);
		}
		cout << endl;
		set_day(days);
	}else{
		cerr << "Wrong number of arguments" << endl;
		exit(1);
	}
	cout << "---" << days[0] << days[1] << days[2] << days[3] << days[4] << days[5] << days[6] << "---\n" << endl;

	timer(hour, minute, days); //(hour, minute, days)
	return 0;
}
