#ifndef _LED_H
#define _LED_H

#include <vector>

using namespace std;

int check_temperature();
vector<string> read_mus_ext( const char * aud_dir );
void play_sound(const char* path);
void* play_list(void* p);
void* led(void* p);
void* butt(void* p);

#endif