#include <stdio.h>
#include <stdlib.h>
#include <wiringPi.h>
#include <iostream>		// cout
#include <iomanip>		// put_time
#include <thread>		// sleep_until, sleep_for
#include <unistd.h> 	// sleep()
#include <unistd.h>	//temperature
#include <fcntl.h>	//temperature
#include <dirent.h>	//temperature
#include <time.h>	//temperature
#include <string.h>	//temperature
#include <cstring>	//play
#include <signal.h>	//kill
#include "peripherals.h"


#define BUTTON 21
//#define BUZZER 22
#define LED01 19
#define LED02 12
#define LED03 26
#define TEMPERATURE_LIMIT 25
#define ALARM_DURATION_SEC 35

using namespace std;

pthread_t tmp_thread_play; // for temporary thread which will be store thread id of second thread 
pthread_t tmp_thread_led; // for temporary thread which will be store thread id of second thread 
int end_led = 0;



int check_temperature(){
    char path[50] = "/sys/bus/w1/devices/";
    char rom[20];
    char buf[100];
    DIR *dirp;
    struct dirent *direntp;
    int fd =-1;
    char *temp;
    float value;

    if((dirp = opendir(path)) == NULL){ // Check if /sys/bus/w1/devices/ exists.
        printf("Temperature: opendir error\n");
    }
    // Reads the directories or files in the current directory.
    while((direntp = readdir(dirp)) != NULL){
        if(strstr(direntp->d_name,"28-")){
            strcpy(rom,direntp->d_name);
        }
    }
    closedir(dirp);
	
    // Append the String rom and "/w1_slave" to path
    // path becomes to "/sys/bus/w1/devices/28-00000xxxx/w1_slave"
    strcat(path,rom);
    strcat(path,"/w1_slave");

    // Open the file in the path.
    if((fd = open(path,O_RDONLY)) < 0){
        printf("Temperature: open error\n");
    }

    // Read the file
    if(read(fd,buf,sizeof(buf)) < 0){
        printf("Temperature: read error\n");
    }
    // Returns the first index of 't'
    temp = strchr(buf,'t');
    // Read the string following "t=".
    sscanf(temp,"t=%s",temp);
    // atof: changes string to float.
    value = atof(temp)/1000;
    printf(" temp : %3.3f °C\n",value);
	
	return value;
}//check_temperature




vector<string> read_mus_ext( const char * aud_dir ){
    struct dirent * aud_file;
    DIR * aud_path;
    vector<string> mus_names;

    if(( aud_path = opendir( aud_dir ) ) ) {
        while(( aud_file = readdir( aud_path ) ) ){
            string a;
            a = string(aud_file->d_name);
            if(a.size() > 4){
                if( (a.compare(a.size()-3, 3, "wav") == 0) || (a.compare(a.size()-3, 3, "mp3") == 0) )
                    mus_names.push_back(a);
            }
        }
        closedir( aud_path );
        if(mus_names.size() == 0){
            cerr << "No audio files. Put some .wav files to " << aud_dir << " directory." << endl;
            exit(0);
        }
    }else{
        cerr << "Failed to open directory " << aud_dir << endl;
        exit(0);
    }

    return mus_names;
}//read_mus_ext




void play_sound(const char* path){
	char buffer[200];
	char *target = strdup(path);
	snprintf(buffer, sizeof(buffer), "omxplayer %s", target );

	system(buffer);

}//play_sound




void* play_list(void* p){
	tmp_thread_play = pthread_self(); 	// store thread_two id to tmp_thread_led 

//	int k = 5; //number of tracks
    time_t t;
    srand(time(&t));
	
	int i = 0;
	string song_name = "zero";
	string tmp_song_name = "zero";
	do{
		int temperature = check_temperature();
		if(temperature >= TEMPERATURE_LIMIT){ //it's hot
			vector<string> mus_names = read_mus_ext("../audio/warm");
			tmp_song_name = song_name;
			song_name = mus_names[rand()%(mus_names.size())];
			if(song_name != tmp_song_name){
				cout << "It's hot, let's play something from playlist \"warm\": " << endl;
				string wav_name = "../audio/warm/" + song_name;
				cout << wav_name << endl;
				play_sound( wav_name.c_str() );
				this_thread::sleep_for(1000ms); //pause
			}
		}else if(temperature < TEMPERATURE_LIMIT){ //it's cold
			vector<string> mus_names = read_mus_ext("../audio/chill");
			tmp_song_name = song_name;
			song_name = mus_names[rand()%(mus_names.size())];
			if(song_name != tmp_song_name){
				cout << "It's cold, let's play something from playlist \"chill\": " << endl;
				string wav_name = "../audio/chill/" + song_name;
				cout << wav_name << endl;
				play_sound( wav_name.c_str() );
				this_thread::sleep_for(1000ms); //pause
			}
		}else{
			printf("Error temperature checking\n");
			vector<string> mus_names = read_mus_ext("../audio");
			tmp_song_name = song_name;
			song_name = mus_names[rand()%(mus_names.size())];
			if(song_name != tmp_song_name){
				string wav_name = "../audio/" + song_name;
				cout << wav_name << endl;
				play_sound( wav_name.c_str() );
				this_thread::sleep_for(1000ms); //pause
			}
		}
		i++;
	}while(end_led != 1);
//	}while(i < k);
	pthread_exit(NULL); // for exit from thread_three
}//play_list




void* led(void* p){	
	tmp_thread_led = pthread_self(); 	// store thread_two id to tmp_thread_led 

	wiringPiSetup();
	digitalWrite(LED01, HIGH);
	digitalWrite(LED02, HIGH);
	digitalWrite(LED03, HIGH);

	while(true){

		using chrono::system_clock; // sleep_until
		time_t now;
		struct tm ptm;
		time(&now);
		ptm = *localtime(&now);
//		cout << "Current time: " << put_time(&ptm,"%X") << '\n';

		ptm.tm_sec = ALARM_DURATION_SEC; // setup alarm duration in secundes
//		cout << "The alarm will stop: " << put_time(&ptm,"%X") << '\n';
		long double differ;
		differ = difftime(now, mktime(&ptm));
//		cout << "There are " << differ << " seconds until the alarm stop" << endl;

//		digitalWrite(BUZZER, LOW);
		delay(100);
		digitalWrite(LED01, HIGH);
		delay(100);
		digitalWrite(LED02, HIGH);
		delay(100);
		digitalWrite(LED03, HIGH);

		delay(500);
//		digitalWrite(BUZZER, HIGH);
		delay(100);
		digitalWrite(LED01, LOW);
		delay(100);
		digitalWrite(LED02, LOW);
		delay(100);
		digitalWrite(LED03, LOW);

		delay(500);

		if(differ >= 0){

			digitalWrite(LED01, HIGH);
			digitalWrite(LED02, HIGH);
			digitalWrite(LED03, HIGH);

			//stop playing track
			system("pgrep omxplayer.bin > pomx.txt"); 
			system("xargs kill < pomx.txt");

			break;
		}//if

	}//while
	digitalWrite(LED01, LOW);
	digitalWrite(LED02, LOW);
	digitalWrite(LED03, LOW);

	end_led = 1;
	pthread_exit(NULL); // for exit from thread_two

}//led




void* butt(void* p) { // thread_one call butt
	while (1){
		end_led == 0;

		int rc = wiringPiSetupGpio();
		if (rc == 0){
			pinMode(BUTTON, INPUT);
			pullUpDnControl(BUTTON, PUD_UP);
			while (1){
				if(!digitalRead(BUTTON)){ //negation (0) - pressing the button
//					digitalWrite(BUZZER, HIGH);
					digitalWrite(LED01, LOW);
					digitalWrite(LED02, LOW);
					digitalWrite(LED03, LOW);

					//stop playing track by killing ID process of omxplayer
					system("pgrep omxplayer.bin > pomx.txt"); 
					system("xargs kill < pomx.txt");

					pthread_cancel(tmp_thread_led); // for cancel thread_two 
					pthread_cancel(tmp_thread_play); // for cancel thread_three
					
					pthread_exit(NULL); // for exit from thread_one
				}//if
				if( end_led == 1 ){
					pthread_exit(NULL); // for exit from thread_one
				}
			}//while
		}//if
	}//while
}//butt
